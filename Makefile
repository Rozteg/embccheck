SRC=src/
SOURCES=main.cpp $(shell find $(SRC) -name '*.cpp')
INCLUDES=./inc/
OBJECTS=$(SOURCES:.cpp=.o)
EXE=embccheck
CINC=-lpthread -lbb_api


all: $(EXE)

#all: t

#t:
#	$(CC) $(CFLAGS) $(INCLUDES) main.c -o test

$(EXE): $(OBJECTS)
	$(CXX) $(CINC) $(LDFLAGS) $(OBJECTS) -I$(INCLUDES) -o $@

.cpp.o:
	$(CXX) $(CFLAGS) -I$(INCLUDES) $< -c -o $@

clean:
	rm -f *~ $(EXE)
	rm -f *.o
	rm -f src/*.o

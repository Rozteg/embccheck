#ifndef _GPIO_H
#define _GPIO_H

#include <cstdio>
#include <iostream>

#include "EMBC1K.h"

#include <unistd.h> // usleep
#include <sys/io.h> // iopl


class Gpio {

private:
    WORD GPIOM;
    WORD GPIO;
    int Open();

public:
    Gpio();
    ~Gpio();
    int Read(int pin);
    int Write(int pin, int value);
};

#endif // _GPIO_H 
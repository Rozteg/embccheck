#ifndef _SERIAL_H
#define _SERIAL_H

#include <cstdio>
#include <iostream>
#include <iterator>
#include <vector>
#include <string>
#include <sstream>
//#include <cstdlib>
#include <fcntl.h>   /* File Control Definitions           */
#include <termios.h> /* POSIX Terminal Control Definitions */
#include <unistd.h>  /* UNIX Standard Definitions 	   */ 
#include <errno.h>   /* ERROR Number Definitions           */
#include <sys/ioctl.h>

class Serial {

private:
    std::vector<char> inVector;
    std::string inc;
    std::string name;
    speed_t speed;

protected:
    int fd;

public:
    Serial(std::string portName, speed_t portSpeed);
    ~Serial();
    int Open();
    void Close();
    int Write(std::string message);
    int WriteFF();
    std::string Read();
    std::vector<std::string> ReadMessages();
};

#endif // _SERIAL_H 
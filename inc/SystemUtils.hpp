#ifndef _SYSTEM_UTILS_H
#define _SYSTEM_UTILS_H

#include <stdio.h>
#include <stdlib.h>

namespace SystemUtils {

    /* usbreset -- send a USB port reset to a USB device */
    int ResetUSB();

};

#endif // _SYSTEM_UTILS_H
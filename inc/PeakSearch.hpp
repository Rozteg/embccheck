#ifndef _PEAK_SEARCH_H
#define _PEAK_SEARCH_H

#include <iostream>
#include <vector>
#include <algorithm>
#include <string>
#include <sstream>

#include "bb_api.h"

namespace SignalHound
{

    double PeakSearch(double center);
    double ParseFreq(std::string msg);

}

#endif // _PEAK_SEARCH_H
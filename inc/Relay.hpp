#ifndef _RELAY_H
#define _RELAY_H

#include <Gpio.hpp>

class Relay
{

private:
    Gpio* gpioChip;
    int gpioPin;

public:
    Relay(Gpio* chip, int pin);
    ~Relay();
    int Close();
    int Open();
};

#endif // _RELAY_H 
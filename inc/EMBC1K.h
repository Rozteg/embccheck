#ifndef __EMBC1K_H__
	#define __EMBC1K_H__

// NOT Support GPIO Function
	//#define _EMBC1K_GPIO_     0
// Support GPIO Function
	#define _EMBC1K_GPIO_     1 // Default
	#define GPIO_PINS        16
// NOT Support WDT Function
	#define _EMBC1K_WDT_      0
// Support WDT Function
	//#define _EMBC1K_WDT_      1 // Default

	#if defined(__GNUC__)
		#include <inttypes.h>

		#if !defined(BOOL)
			#define BOOL  int
			#define TRUE  1
			#define FALSE 0
		#endif
		#if !defined(BYTE)
			#define BYTE  uint8_t
		#endif
		#if !defined(WORD)
			#define WORD  uint16_t
		#endif
		#if !defined(DWORD)
			#define DWORD uint32_t
		#endif
		#if !defined(LONG)
			#define LONG  int32_t
		#endif
	#elif defined(__TURBOC__)
		#if !defined(BOOL)
			#define BOOL  int
			#define TRUE  1
			#define FALSE 0
		#endif
		#if !defined(BYTE)
			#define BYTE  unsigned char
		#endif
		#if !defined(WORD)
			#define WORD  unsigned short
		#endif
		#if !defined(DWORD)
			#define DWORD unsigned long
		#endif
		#if !defined(LONG)
			#define LONG  long
		#endif
	#endif

	#if !defined(_EMBC1K_GPIO_) || _EMBC1K_GPIO_ || !defined(_EMBC1K_WDT_) || _EMBC1K_WDT_
BOOL EMBC1K_Initial();
	#endif
	#if !defined(_EMBC1K_GPIO_) || _EMBC1K_GPIO_
BOOL EMBC1K_GetGPIOConfig(WORD *Mask);
BOOL EMBC1K_SetGPIOConfig(WORD Mask);
BOOL EMBC1K_GetGPIO(WORD *DI);
BOOL EMBC1K_SetGPIO(WORD DO);
	#endif
	#if !defined(_EMBC1K_WDT_) || _EMBC1K_WDT_
BOOL EMBC1K_GetWDT(DWORD *WDT);
BOOL EMBC1K_SetWDT(DWORD WDT);
BOOL EMBC1K_CancelWDT();
	#endif
#endif

#include "EMBC1K.h"

#if !defined(_EMBC1K_GPIO_) || _EMBC1K_GPIO_ || !defined(_EMBC1K_WDT_) || _EMBC1K_WDT_
	#define _IT8786_
#endif

#if defined(_IT8783_) || defined(_IT8786_)
	#define _MB_PNP_MODE_    0
	#define _IT_KRST_
#endif

#if defined(__GNUC__)
	//#include <unistd.h>    // usleep
	#if !defined(_EMBC1K_GPIO_) || _EMBC1K_GPIO_ || !defined(_EMBC1K_WDT_) || _EMBC1K_WDT_
		#include <sys/io.h>    // iopl, outb, inb, outl, inl
	#endif

	//#include <stdio.h>// printf, fscanf
#elif defined(__TURBOC__)
	#if !defined(_EMBC1K_GPIO_) || _EMBC1K_GPIO_ || !defined(_EMBC1K_WDT_) || _EMBC1K_WDT_
		#include <dos.h>  // outportb, inportb, delay
	#endif

	//#include <stdio.h>// printf, fscanf
#endif

#if defined(__GNUC__)
	#if !defined(_EMBC1K_GPIO_) || _EMBC1K_GPIO_ || !defined(_EMBC1K_WDT_) || _EMBC1K_WDT_
		#define Inp                    inb
		#define Outp(_a_, _d_)         outb(_d_, _a_)
	#endif
	//#define Inpd                   inl
	//#define Outpd(_a_, _d_)        outl(_d_, _a_)
	//#define Sleep(_t_)             usleep((_t_) * 1000)

	//#define NEW_LINE               "\n"
#elif defined(__TURBOC__)
	#if !defined(_EMBC1K_GPIO_) || _EMBC1K_GPIO_ || !defined(_EMBC1K_WDT_) || _EMBC1K_WDT_
		//#define ioperm(_a_, _p_, _e_)
		#define iopl(_p_)
		#define Inp                    inportb
		#define Outp                   outportb
	#endif
/*
DWORD Inpd(WORD p){
  DWORD dwData;
  asm mov dx, p;
  asm lea bx, dwData;
  __emit__(
    0x66, 0x50, // push EAX
    0x66, 0xED, // in EAX,DX
    0x66, 0x89, 0x07, // mov [BX],EAX
    0x66, 0x58); // pop EAX
  return dwData;
}

void Outpd(WORD p, DWORD dwData){
  asm mov dx, p;
  asm lea bx, dwData;
  __emit__(
    0x66, 0x50, // push EAX
    0x66, 0x8B, 0x07, // mov EAX,[BX]
    0x66, 0xEF, // out DX,EAX
    0x66, 0x58); // pop EAX
}
*/
	//#define Sleep                  delay

	//#define NEW_LINE               "\r\n"
#endif

#if !defined(_EMBC1K_GPIO_) || _EMBC1K_GPIO_ || !defined(_EMBC1K_WDT_) || _EMBC1K_WDT_
static  BYTE  Init = FALSE;
#endif

#if defined(_IT8783_)
	#define ITECHIPIDNO 0x8783
#elif defined(_IT8786_)
	#define ITECHIPIDNO 0x8786
#endif

#if defined(_IT8783_) || defined(_IT8786_)
//extern  WORD  ITEChipID[4];
static  WORD  ITEChipID[4];
	#if !defined(_EMBC1K_GPIO_) || _EMBC1K_GPIO_
static  WORD  ITESIOAddr = 0;
	#endif
	#if !defined(_MB_PNP_MODE_) || !_MB_PNP_MODE_
		#define ITECHIPID ITEChipID[0]
		#define MBPNPADDR 0x55
		#define SIOAddrP  0x2E
		#define SIODataP  0x2F
	#elif _MB_PNP_MODE_ == 1
		#define ITECHIPID ITEChipID[2]
		#define MBPNPADDR 0xAA
		#define SIOAddrP  0x4E
		#define SIODataP  0x4F
	#endif
#endif

#if !defined(_MULTIPROC_) && (defined(_IT8783_) || defined(_IT8786_) || defined(_SMB_))
	#if !defined(_EMBC1K_GPIO_) || _EMBC1K_GPIO_
static  WORD  LastGPOM = 0;
	#endif
#endif
#if !defined(_EMBC1K_GPIO_) || _EMBC1K_GPIO_
static  WORD  GPOM = 0;
#endif

#if !defined(_EMBC1K_GPIO_) || _EMBC1K_GPIO_ || !defined(_EMBC1K_WDT_) || _EMBC1K_WDT_
BOOL EMBC1K_Initial(){
	#if defined(_IT8783_) || defined(_IT8786_) || defined(_SMB_)
	BYTE  ab[2] = { 0 };
		#if defined(_IT8783_) || defined(_IT8786_)
// <<<< need to add mutex for initial ITE SIO Chip
	if (ITECHIPID && ITECHIPID != ITECHIPIDNO) return FALSE;
// Enter Super io
	Outp(0x2E, 0x87);
	Outp(0x2E, 0x01);
	Outp(0x2E, 0x55);
	Outp(0x2E, MBPNPADDR);
	//Sleep(10);
// Chip Version // Select Chip // By IT
	Outp(SIOAddrP, 0x22);
	Outp(SIODataP, 0x80);
	//Sleep(10);
	Outp(SIOAddrP, 0x20);
	ITECHIPID = Inp(SIODataP) << 8;
	Outp(SIOAddrP, 0x21);
	ITECHIPID |= Inp(SIODataP);
	if (ITECHIPID != ITECHIPIDNO) return FALSE;
// Set Logic Device
	Outp(SIOAddrP, 0x07);
	Outp(SIODataP, 0x07);
// Set Special Function Register 2 // By IT
			#if defined(_IT8786_)
	Outp(SIOAddrP, 0x2B);
	Outp(SIODataP, 0x48);
// External VIN3 voltage sensor
	Outp(SIOAddrP, 0x2C);
	ab[1] = (ab[0] = Inp(SIODataP)) & ~0x01;
	if (ab[0] != ab[1]) Outp(SIODataP, ab[1]);
			#endif
// Set Select GPIO Pin
			#if !defined(_EMBC1K_GPIO_) || _EMBC1K_GPIO_ || !defined(_EMBC1K_WDT_) || _EMBC1K_WDT_
				#if defined(_IT8786_)
	Outp(SIOAddrP, 0x29);
	ab[1] = (ab[0] = Inp(SIODataP))
					#if defined(_IT_KRST_)
	                                & ~0x40;
					#elif !defined(_IT_KRST_)
	                                | 0x40;
					#endif
	if (ab[0] != ab[1]) Outp(SIODataP, ab[1]);
				#endif
			#endif
// Set Simple IO Enable
			#if !defined(_EMBC1K_GPIO_) || _EMBC1K_GPIO_
				#if defined(_IT8786_)
	Outp(SIOAddrP, 0x07);
	Outp(SIODataP, 0x03);
	//Sleep(10);
// GP7
	Outp(SIOAddrP, 0x30);
	Outp(SIODataP, 0x00);
// GP8
	Outp(SIOAddrP, 0x2C);
	ab[1] = (ab[0] = Inp(SIODataP)) | 0x80;
	if (ab[0] != ab[1]) Outp(SIODataP, ab[1]);
	Outp(SIOAddrP, 0x07);
	Outp(SIODataP, 0x07);
	//Sleep(10);
				#endif
			#endif
// Get GPIO IO Select
			#if !defined(_EMBC1K_GPIO_) || _EMBC1K_GPIO_
// GP7[0:7]
	Outp(SIOAddrP, 0xCE);
	GPOM = (ab[0] = Inp(SIODataP)) << 8 & 0xF000 | ab[0] << 4 & 0x00F0;
// GP8[0:7]
	Outp(SIOAddrP, 0xCF);
	GPOM |= (ab[0] = Inp(SIODataP)) << 4 & 0x0F00 | ab[0] & 0x000F;
				#if !defined(_MULTIPROC_)
	LastGPOM = GPOM;
				#endif
			#endif
			#if !defined(_EMBC1K_GPIO_) || _EMBC1K_GPIO_
// Get Simple Base Address
	Outp(SIOAddrP, 0x62);
	ITESIOAddr = Inp(SIODataP) << 8;
	Outp(SIOAddrP, 0x63);
	ITESIOAddr |= Inp(SIODataP);
			#endif
/*
			#if !defined(_EMBC1K_GPIO_) || _EMBC1K_GPIO_
// Clear Output
	if (ITESIOAddr){
// <<<< need to add mutex 1st check for Clear Output
// GP7[0:7]
		Outp(ITESIOAddr + 6, 0x00);
// GP8[0:7]
		Outp(ITESIOAddr + 7, 0x00);
	}
			#endif
*/
		#endif
	#endif
	Init = TRUE;
	return TRUE;
}
#endif

#if !defined(_EMBC1K_GPIO_) || _EMBC1K_GPIO_
BOOL EMBC1K_GetGPIOConfig(WORD *Mask){
	if (!Init || !Mask
	#if defined(_IT8783_) || defined(_IT8786_)
	 || !ITESIOAddr || ITECHIPID != ITECHIPIDNO
	#endif
	) return FALSE;
	*Mask = GPOM;
	return TRUE;
}

BOOL EMBC1K_SetGPIOConfig(WORD Mask){
	#if defined(_IT8783_) || defined(_IT8786_)
	BYTE  ab[1];
	#endif
	if (!Init
	#if defined(_IT8783_) || defined(_IT8786_)
	 || !ITESIOAddr || ITECHIPID != ITECHIPIDNO
	#endif
	) return FALSE;
	#if defined(_IT8783_) || defined(_IT8786_)
	ab[0] = Mask >> 4 & 0xF0 | Mask & 0x0F;
		#if !defined(_MULTIPROC_)
	if (ab[0] != (LastGPOM >> 4 & 0xF0 | LastGPOM & 0x0F))
		#endif
	{
// GP8[0:7]
		Outp(SIOAddrP, 0xCF);
		Outp(SIODataP, ab[0]);
	}
	ab[0] = Mask >> 8 & 0xF0 | Mask >> 4 & 0x0F;
		#if !defined(_MULTIPROC_)
	if (ab[0] != (LastGPOM >> 8 & 0xF0 | LastGPOM >> 4 & 0x0F))
		#endif
	{
// GP7[0:7]
		Outp(SIOAddrP, 0xCE);
		Outp(SIODataP, ab[0]);
	}
		#if !defined(_MULTIPROC_)
	LastGPOM = Mask;
		#endif
	#endif
	GPOM = Mask;
	return TRUE;
}

BOOL EMBC1K_GetGPIO(WORD *DI){
	#if defined(_IT8783_) || defined(_IT8786_)
	BYTE  ab[1];
	#endif
	if (!Init || !DI
	#if defined(_IT8783_) || defined(_IT8786_)
	 || !ITESIOAddr || ITECHIPID != ITECHIPIDNO
	#endif
	) return FALSE;
	#if defined(_IT8783_) || defined(_IT8786_)
		#if defined(_MULTIPROC_)
// GP7[0:7]
	Outp(SIOAddrP, 0xCE);
	Outp(SIODataP, GPOM >> 8 & 0xF0 | GPOM >> 4 & 0x0F);
// GP8[0:7]
	Outp(SIOAddrP, 0xCF);
	Outp(SIODataP, GPOM >> 4 & 0xF0 | GPOM & 0x0F);
		#endif
	*DI = (ab[0] = Inp(ITESIOAddr + 6)) << 8 & 0xF000 | ab[0] << 4 & 0x00F0;
	*DI |= (ab[0] = Inp(ITESIOAddr + 7)) << 4 & 0x0F00 | ab[0] & 0x000F;
	#endif
	return TRUE;
}

BOOL EMBC1K_SetGPIO(WORD DO){
	#if defined(_IT8783_) || defined(_IT8786_)
	BYTE  ab[2];
	#endif
	if (!Init
	#if defined(_IT8783_) || defined(_IT8786_)
	 || !ITESIOAddr || ITECHIPID != ITECHIPIDNO
	#endif
	) return FALSE;
	#if defined(_IT8783_) || defined(_IT8786_)
	ab[0] = GPOM >> 8 & 0xF0 | GPOM >> 4 & 0x0F;
		#if defined(_MULTIPROC_)
// GP7[0:7]
	Outp(SIOAddrP, 0xCE);
	Outp(SIODataP, ab[0]);
		#endif
	if (ab[0]){
		if (ab[0] != 0xFF) ab[1] = Inp(ITESIOAddr + 6);
		Outp(ITESIOAddr + 6, ab[1] & ~ab[0] | DO >> 8 & ab[0] & 0xF0 | DO >> 4 & ab[0] & 0x0F);
	}
	ab[0] = GPOM >> 4 & 0xF0 | GPOM & 0x0F;
		#if defined(_MULTIPROC_)
// GP8[0:7]
	Outp(SIOAddrP, 0xCF);
	Outp(SIODataP, ab[0]);
		#endif
	if (ab[0]){
		if (ab[0] != 0xFF) ab[1] = Inp(ITESIOAddr + 7);
		Outp(ITESIOAddr + 7, ab[1] & ~ab[0] | DO >> 4 & ab[0] & 0xF0 | DO & ab[0] & 0x0F);
	}
	#endif
	return TRUE;
}
#endif


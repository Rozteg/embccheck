#include <Serial.hpp>

using namespace std;

Serial::Serial(string portName, speed_t portSpeed)
{
    name = portName;
    fd = 0;
    inVector.resize(4096);
    speed = portSpeed;
    inc = "";
}

Serial::~Serial()
{
    Close();
}

int Serial::Open()
{
    Close();

    fd = open(name.c_str(), O_RDWR | O_NOCTTY | O_NDELAY);

    if(fd == -1) /* Error Checking */
    {
	cerr << "Error! in Opening " << name  << " \n\r";
	return -1;
    }
    else
	cout << name << " Opened Successfully \n\r";

    struct termios SerialPortSettings;	/* Create the structure                          */

    tcgetattr(fd, &SerialPortSettings);	/* Get the current attributes of the Serial port */

    cfsetispeed(&SerialPortSettings, speed); /* Set Read Speed                       */
    cfsetospeed(&SerialPortSettings, speed); /* Set Write Speed                      */

    SerialPortSettings.c_cflag &= ~PARENB;   /* Disables the Parity Enable bit(PARENB),So No Parity   */
    SerialPortSettings.c_cflag &= ~CSTOPB;   /* CSTOPB = 2 Stop bits,here it is cleared so 1 Stop bit */
    SerialPortSettings.c_cflag &= ~CSIZE;	 /* Clears the mask for setting the data size             */
    SerialPortSettings.c_cflag |=  CS8;      /* Set the data bits = 8                                 */

    SerialPortSettings.c_cflag &= ~CRTSCTS;       /* No Hardware flow Control                         */
    SerialPortSettings.c_cflag |= CREAD | CLOCAL; /* Enable receiver,Ignore Modem Control lines       */ 


    SerialPortSettings.c_iflag &= ~(IXON | IXOFF | IXANY);          /* Disable XON/XOFF flow control both i/p and o/p */
    SerialPortSettings.c_iflag &= ~(ICANON | ECHO | ECHOE | ISIG);  /* Non Cannonical mode                            */

    SerialPortSettings.c_oflag &= ~OPOST; /*No Output Processing*/

    cfmakeraw(&SerialPortSettings); // can be useful

    SerialPortSettings.c_cc[VMIN] = 0;
    SerialPortSettings.c_cc[VTIME] = 0;

    if((tcsetattr(fd, TCSANOW, &SerialPortSettings)) != 0) /* Set the attributes to the termios structure*/
    {
	cout << " ERROR ! in Setting attributes \n\r";
	close(fd);
	return -1;
    }


    return fd;
}

void Serial::Close()
{
    if (fd > 0)
    {
        close(fd);
        fd = 0;
    }
}

int Serial::Write(string message)
{
    message = message + "\n";
    return write(fd, message.c_str(), message.size());
}


int Serial::WriteFF()
{
    vector<char> send;
    send.push_back(0xFF);
    //send.push_back(0xFF);
    //send.push_back(0x00);
    //send.push_back(0x00);
    return write(fd, &(*send.begin()), send.size());
}

string Serial::Read()
{
    string incoming;
    int bytesRead;
    bytesRead = read(fd, &(*inVector.begin()), inVector.size());

    if (bytesRead > 0)
    {
	incoming = string(inVector.begin(), inVector.begin() + bytesRead);
	//cout << "read bytes " << bytesRead << "\n";
    }

    return incoming;
}

vector<string> Serial::ReadMessages()
{
    vector<string> messages;
    string token;

//    if (inc != "")
//	cout << "not empty ! "<< inc <<"  \r\n";

    inc += Read();

//    if (inc != "")
//	cout << "GOT !\r\n";

    stringstream ss(inc);
    while (getline(ss, token, '\n'))
    {
//	cout << token << '\n';
	
	if (token != "")
	{
    	    messages.push_back(token);
    	    cout << token << endl;
    	}
    }


    if (messages.size() != 0)
    {
	if (token == "")
	    inc.clear();
	else
	    inc = token;
    }


    //inc.clear();

    return messages;
}



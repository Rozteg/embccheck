#include "PeakSearch.hpp"
#include "SystemUtils.hpp"

using namespace std;

double SignalHound::PeakSearch(double center)
{
    bbStatus status;
    int handle = -1;
    int count = 0;
    int limit = 2;

    for (;; count++)
    {
	status = bbOpenDevice(&handle);
	if(status != bbNoError)
	{
	    // Unable to open device
	    cout << "Unable to open device" << endl;
	    cout << bbGetErrorString(status) << endl;
	    if (count >= limit)
	    {
		cout << "Totally unable to open device" << endl;
		return 0;
	    }
	    else
	    {
		cout << "Resetting USB" << endl;
		SystemUtils::ResetUSB();
	    }
	}
	else
	{
	    cout << "Device opened successfully\n";
	    break;
	}
    }

    vector<double> min, max;
    unsigned int len;
    double start, bin;

    // double center = 0;
    double width = 0.5e9; //5.98e9; //500 MHz

    bbConfigureAcquisition(handle, BB_MIN_AND_MAX, BB_LOG_SCALE);
    bbConfigureCenterSpan(handle, center, width);
    bbConfigureLevel(handle, -20.0, BB_AUTO_ATTEN);
    bbConfigureGain(handle, BB_AUTO_GAIN);
    bbConfigureSweepCoupling(handle, 100.0e3, 100.0e3, 0.001, 
			   BB_NON_NATIVE_RBW, BB_NO_SPUR_REJECT);

    bbInitiate(handle, BB_SWEEPING, 0);
    bbQueryTraceInfo(handle, &len, &bin, &start);

    cout << "Bin size: " << bin << endl;

    min.resize(len);
    max.resize(len);

    bbFetchTrace(handle, len, &min[0], &max[0]);

    vector<double>::iterator iter = max_element(max.begin(), max.end());

    double peakFreq = 0;
    peakFreq = (start + bin * (iter - max.begin())) * 1.0e-6;

    cout << "Peak Freq = "
	<< peakFreq
	<< " MHz"
	<< endl;
    cout << "Peak Amp = "
	<< *iter << " dBm"
	<< endl;

    bbAbort(handle);
    bbCloseDevice(handle);

    return peakFreq;
}

double SignalHound::ParseFreq(string msg)
{
    string toParse = msg.substr(0, msg.size() - 1);
    string token;

    stringstream ss(toParse);
    while (getline(ss, token, '|'))
    { }

    double freq = stod(token);

    cout << "Freq: " << freq << " MHz" << endl;

    return freq;
}
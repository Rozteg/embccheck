#include <Gpio.hpp>

using namespace std;


// input pins 0-3 (jdio 1 [80 - 83]) ; 8-11 (jdio 2 [84 - 87])
// output pins 4-7 (jdio 1 [70 - 73]) ; 12-15 (jdio 2 [74 - 77])

Gpio::Gpio()
{
    Open();
}

Gpio::~Gpio()
{

}

int Gpio::Open()
{
    WORD InitialState = 0x0000;

    iopl(3);
    int init =  EMBC1K_Initial(); // default mode f0f0 , 0 - input, 1 - output
    EMBC1K_GetGPIOConfig(&GPIOM);

    EMBC1K_SetGPIO(InitialState);

    return init;
}



int Gpio::Read(int pin)
{
    if (pin > GPIO_PINS || pin < 0)
	return -1;

    EMBC1K_GetGPIO(&GPIO);

    if ((1 << pin) & GPIO)
	return 1;
    else
	return 0;
}

int Gpio::Write(int pin, int value)
{
    int ret;

    if (pin >= GPIO_PINS || pin < 0)
	return -1;

    EMBC1K_GetGPIO(&GPIO);


    if (value == 0)
    {
	GPIO &= ~(1 << pin);
	ret = EMBC1K_SetGPIO(GPIO);
    }
    else if (value == 1)
    {
	GPIO |= (1 << pin);
	ret = EMBC1K_SetGPIO(GPIO);
    }
    else
	ret = -1;

    return ret;
}

#include <sstream>
#include <pthread.h>

#include "Serial.hpp"
#include "Gpio.hpp"
#include "Relay.hpp"
#include "PeakSearch.hpp"

using namespace std;

// input pins 0-3 (jdio 1 [80 - 83]) ; 8-11 (jdio 2 [84 - 87])
// output pins 4-7 (jdio 1 [70 - 73]) ; 12-15 (jdio 2 [74 - 77])

struct relayThreadStruct {
    Gpio* gpio;
    int dkPin;
    int grPin;
    Relay* relay;
};

void* relay_thread(void* in_data)
{
    // IF GERKON OTKL RELE OTKL
    // IF DK 3 SEK RELE OTKL 3.6 SEK

    struct relayThreadStruct* data;
    data = (struct relayThreadStruct *) in_data;

    int prevDk = 0;
    int prevGr = 0;

    int dkCounter = 0;

    int alarmCounter = 0;

    while(1)
    {
	usleep(100000); // 1/10 of a second
	int grState = data->gpio->Read(data->grPin);

	if (grState != prevGr)
	{
	    if (grState)
		data->relay->Close();
	    else
		data->relay->Open();
	    prevGr = grState;
	}

	int dkState = data->gpio->Read(data->dkPin);
	dkCounter += dkState;
	
	if (dkState == 0)
	{
	    if ((dkCounter > 20) && (dkCounter < 50))
	    {
		alarmCounter = 37;
		data->relay->Open();
	    }

	    dkCounter = 0;
	}

	if (alarmCounter > 0)
	{
	    alarmCounter -= 1;
	    if (alarmCounter == 0)
		data->relay->Close();
	}
    }

    pthread_exit(NULL);
}


int main(int argc, char **argv)
{
    Gpio GPIO;

    //Serial L1("/dev/ttyS0", B115200); // Works
    Serial L1("/dev/ttyS1", B9600); // Works
    L1.Open();

    Serial L2("/dev/ttyS2", B9600); // Works
    L2.Open();

    Relay R1(&GPIO, 12);
    R1.Close();

    GPIO.Write(13, 1);

    struct relayThreadStruct data;
    data.gpio = &GPIO;
    data.dkPin = 10;
    data.grPin = 11;
    data.relay = &R1;

    pthread_t Rthread;
    if(pthread_create(&Rthread, NULL, relay_thread, (void *) &data))
	cout << "Unable to create thread" << endl;
    else
	pthread_detach(Rthread);
    /* thread init end  */

    /* actual check */

    vector<string> messages;
    messages.reserve(10);

    while (1)
    {
	usleep(200000);
	vector<string> inc1 = L1.ReadMessages();
	vector<string> inc2 = L2.ReadMessages();

	messages.insert(messages.end(), inc1.begin(), inc1.end());
	messages.insert(messages.end(), inc2.begin(), inc2.end());

	for (int i = 0; i < messages.size(); i++)
	{
	    //cout << messages[i] << "\n";

	    if (messages[i] == "ECHO")
	    {
		L1.Write("ECHO");
		L2.Write("ECHO");
	    }

	    if (messages[i] == "RELAY1")
		R1.Close();

	    if (messages[i] == "RELAY0")
		R1.Open();

	    if (messages[i].length() > 0)
	    {
		if ((messages[i].find("SCAN") == 0) && (messages[i].back() == '|'))
		{
		    string freq = "0";
		    ostringstream strs;
		    try
		    {
			strs << SignalHound::PeakSearch(SignalHound::ParseFreq(messages[i]) * 1e6);
		    }
		    catch (...)
		    {
			cout << "Failed to parse frequency" << endl;
		    }
		    freq = strs.str();
		    L1.Write(freq);
		    L2.Write(freq);
		}
	    }
	}

	messages.clear();
    }

    R1.Open();
    L1.Close();
    L2.Close();

    return 0;

}

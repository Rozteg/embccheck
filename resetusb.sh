#!/bin/bash

for i in /sys/bus/pci/drivers/[uoex]hci_hcd/*:* ; do
  [ -e "$i" ] || continue
  echo "${i##*/}" | tee "${i%/*}/unbind"
  echo "${i##*/}" | tee "${i%/*}/bind"
done
sleep 3
echo -ne '\007'

